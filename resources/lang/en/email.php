<?php

return [
    'confirmMessage' => 'Your message has been sent.',
    'messageFrom' => 'Contact form',
    'messageSubject' => 'Message from Infinitycode.pl',
];
