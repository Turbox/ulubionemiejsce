<?php

return [
    'confirmMessage' => 'Wiadomość została wysłana.',
    'emailExists' => 'Nie udał sie zapis bo email istnieje juz w bazie danych.',
    'messageFrom' => 'Potwierdzenie zapisania sie na newsletter.',
    'messageSubjectConfirm' => 'Potwierdź zapis na listę ULUBIONE MIEJSCE.',
];
