<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <!--[if gte mso 9]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml> <![endif]-->
    <style type="text/css">
        #outlook a{padding:0;} /* Force Outlook to provide a "view in browser" message */
        .ReadMsgBody{width:100%;} .ExternalClass{width:100%;} /* Force Hotmail to display emails at full width */
        .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;} /* Force Hotmail to display normal line spacing */
        body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;} /* Prevent WebKit and Windows mobile changing default text sizes */
        table, td{mso-table-lspace:0pt; mso-table-rspace:0pt;} /* Remove spacing between tables in Outlook 2007 and up */
        .img-double{max-width: 50%;}
        img{-ms-interpolation-mode:bicubic;} /* Allow smoother rendering of resized image in Internet Explorer */
        @media only screen and (max-width: 600px) {
            table, td{
                width: auto !important;
            }
            img{
                max-width: 100%;
                height: auto;
            }
            .mobile { display:block !important; max-height:inherit !important; overflow:visible !important; height:auto !important;font-size:10px !important;line-height:12px !important; padding: 20px !important;}
            .mobileFull { width:100%!important; padding:0!important; text-align: center; overflow:hidden; }
            .dn {
                display:none;
            }
        }
        a {
            color: #000000 !important;
        }
        .button {
            text-decoration:none;
            width: 100px;
            height: 40px;
            border: none;
            border-radius: 20px;
            background: #F7ABAD;
            color: #000000 !important;
            font-size: 16px;
            font-weight: 600;
            /*display:block;*/
            padding:10px;
            line-height:20px;
        }
    </style>
</head>
<body style="margin: 0 auto; padding: 0; font-size: 12px; font-family: Arial, sans-serif; background:#FFFFFF;">
<table style="width: 1000px;" border="0" cellspacing="0" cellpadding="0" class="mobileFull">
    <table width="100%" cellpadding="0" cellspacing="0" border="0" style="background:#F8F8F8;">
        <tr>
            <td>
                <center>
                    <table style="width: 600px;background:#FFFFFF;" border="0" cellspacing="0" cellpadding="0" class="mobileFull">
                        <thead>
                        <tr>
                            <td>
                                <center>
                                    <img alt="logo" src="http://ulubionemiejsce.pl/img/banner.png" style="border: 0;">
                                </center>
                            </td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td style="padding: 10px 10px 0;" class="mobile">
                                <h2 style="font-size: 18px; font-family: 'Open Sans', sans-serif; color: #000; margin-top: 0; line-height: 20px; text-align: justify;">
                                    Drogi przedsiębiorco!
                                </h2>
                                <p style="font-size: 14px; font-family: 'Open Sans', sans-serif; color: #000; margin-top: 0; line-height: 20px; text-align: justify;">
                                    Cieszymy się, że chcesz dołączyć do naszej akcji.
                                </p>
                                <p style="font-size: 14px; font-family: 'Open Sans', sans-serif; color: #000; margin-top: 0; line-height: 20px; text-align: justify;">
                                    Jedyne, co musisz teraz zrobić, to potwierdzić zapis na listę.
                                    Wystarczy, że klikniesz na poniższy przycisk, a my będziemy mogli poinformować cię o postępach projektu.
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 20px;" class="mobile">
                                <p style="font-size: 14px; font-family: 'Open Sans', sans-serif; color: #000; margin-top: 0; line-height: 20px; text-align: justify;">
                                    <a class="button" href="{!! route('newsletter.check', [$hash, 'confirm'])  !!}">Potwierdzam</a>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 10px 10px 0;" class="mobile">
                                <p style="font-size: 14px; font-family: 'Open Sans', sans-serif; color: #000; margin-top: 0; line-height: 20px; text-align: justify;">
                                    Daj znać swoim klientom, że mogą wesprzeć swoje #ulubionemiejsce, które stworzyłeś.
                                </p>
                                <p style="font-size: 14px; font-family: 'Open Sans', sans-serif; color: #000; margin-top: 0; line-height: 20px; text-align: justify;">
                                    Jeśli chcesz, polub nas na <a style="color: #1A49BA !important;" href="https://www.facebook.com/projektulubionemiejsce">Facebooku</a> lub
                                    <a style="color: #1A49BA !important;" href="https://www.instagram.com/ulubionemiejsce/">Instagramie</a>, żeby być na bieżąco.
                                </p>
                                <p style="font-size: 14px; font-family: 'Open Sans', sans-serif; color: #000; margin-top: 0; line-height: 20px; text-align: justify;">
                                    Jeśli otrzymałeś/łaś tą wiadomość przez pomyłkę, po prostu ją zignoruj lub usuń.
                                    Nie wpiszemy cię na listę subskrybentów, jeśli sobie tego nie życzysz i nie potwierdzisz zapisu.
                                </p>
                            </td>
                        </tr>
                        </tbody>
                        <tfoot style="font-family: 'Open Sans', sans-serif; font-size: 12px; max-width: 600px; width: 100%; background:#FFFFFF;" border="0" cellspacing="0" cellpadding="0" class="mobileFull">
                        <tr>
                            <td style="margin-top: 20px;">
                                <table style="margin-top: 20px;" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <table style="margin-top: 20px;" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td class="dn" style="font-family: 'Open Sans', sans-serif; font-size: 12px; padding: 0 20px 0 20px; text-align:right; width: 500px">
                                                        <p style="margin: 0; padding: 0"></p>
                                                    </td>
                                                    <td style="font-family: 'Open Sans', sans-serif; font-size: 12px; padding: 0 20px 0 20px; text-align:right; width: 500px; color: #000000;">
                                                        <p style="margin: 0; padding: 0">Copyright © 2020 Zespół Projektu Korona</p></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-family: Arial, sans-serif; font-size: 12px; padding: 20px 20px 0; font-size: 10px; color: #C5C5C5; line-height: 12px; max-width: 600px; width: 100%; text-align:justify;">
                                <p>
                                    Administratorem twoich danych osobowych jest Infinity Code Michał Rogalski, NIP: 501-00-55-732. Podstawą prawną do przetwarzania
                                    danych jest Twoja zgoda, którą wyraziłeś podczas zapisu na newsletter według art. 6 ust. 1 lit. a RODO. Więcej informacji znajdziesz w
                                    <a href="{{route('home.privacyPolicy')}}">polityce prywatności.</a>
                                </p>
                                <p>
                                    Pamiętaj, że w każdej chwili możesz wypisać się z listy, klikając w przycisk:
                                    <a href="{!! route('newsletter.check', [$hash, 'unsub'])  !!}">Wypisuję się</a>
                                </p>
                            </td>
                        </tr>
                        </tfoot>
                    </table>
                </center>
            </td>
        </tr>
    </table>
</table>
</body>
</html>
