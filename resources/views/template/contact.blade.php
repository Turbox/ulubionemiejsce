<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <!--[if gte mso 9]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml> <![endif]-->
    <style type="text/css">
        #outlook a{padding:0;} /* Force Outlook to provide a "view in browser" message */
        .ReadMsgBody{width:100%;} .ExternalClass{width:100%;} /* Force Hotmail to display emails at full width */
        .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;} /* Force Hotmail to display normal line spacing */
        body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;} /* Prevent WebKit and Windows mobile changing default text sizes */
        table, td{mso-table-lspace:0pt; mso-table-rspace:0pt;} /* Remove spacing between tables in Outlook 2007 and up */
        .img-double{max-width: 50%;}
        img{-ms-interpolation-mode:bicubic;} /* Allow smoother rendering of resized image in Internet Explorer */
        @media only screen and (max-width: 600px) {
            table, td{
                width: auto !important;
            }
            img{
                max-width: 100%;
                height: auto;
            }
            .mobile { display:block !important; max-height:inherit !important; overflow:visible !important; height:auto !important;font-size:10px !important;line-height:12px !important; padding: 20px !important;}
            .mobileWidthFull { width:100%!important; padding:0!important; text-align: center; overflow:hidden; }
        }
    </style>
</head>
<body style="margin: 0 auto; padding: 0; font-size: 12px; font-family: Arial, sans-serif; background:#F8F8F8;">
<table style="width: 600px;" border="0" cellspacing="0" cellpadding="0" class="mobileWidthFull">
    <table width="100%" cellpadding="0" cellspacing="0" border="0" style="background:#F8F8F8;">
        <tr>
            <td>
                <center>
                    <table style="width: 600px;background:#FFFFFF;" border="0" cellspacing="0" cellpadding="0" class="mobileWidthFull">
                        <thead>
                        <tr>
                            <td>
                                <center>
                                    <img alt="logo" src="http://infinitycode.pl/images/logo_small.png" style="border: 0;">
                                </center>
                            </td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td style="padding: 40px 10px 0;" class="mobile">
                                <p style="font-size: 14px; font-family: Arial, sans-serif; color: #000; margin-top: 0; line-height: 20px; text-align: justify;">
                                    Your personal data:
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 10px 10px 0;" class="mobile">
                                <p style="font-size: 14px; font-family: Arial, sans-serif; color: #000; margin-top: 0; line-height: 20px; text-align: justify;">
                                    Name: {!! $user['name'] !!}
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 10px 10px 0;" class="mobile">
                                <p style="font-size: 14px; font-family: Arial, sans-serif; color: #000; margin-top: 0; line-height: 20px; text-align: justify;">
                                    E-mail: {!! $user['email'] !!}
                                </p>
                            </td>
                        </tr>
                        <tr><td style="height: 20px"></td></tr>
                        </tbody>
                        <tfoot style="font-family: Arial, sans-serif; font-size: 12px; max-width: 600px; width: 100%; background:#FFFFFF;" border="0" cellspacing="0" cellpadding="0" class="mobileWidthFull">
                        <tr>
                            <td style="margin-top: 20px;">
                                <table style="margin-top: 20px;" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td style="font-family: Arial, sans-serif; font-size: 12px; padding: 0 20px 0 20px; text-align:right; width: 300px"><p style="margin: 0; padding: 0"></td>
                                        <td style="font-family: Arial, sans-serif; font-size: 12px; padding: 0 20px 0 20px; text-align:right; width: 300px"><p style="margin: 0; padding: 0">Copyright © 2020 Infinity Code</p></td>
                                    </tr>
                                </table>
                            </td>

                        </tr>
                        <tr>
                            <td style="font-family: Arial, sans-serif; font-size: 12px; padding: 20px 20px 0; font-size: 10px; color: #C5C5C5; line-height: 12px; max-width: 600px; width: 100%; text-align:justify;">
                                <p>
                                    Niniejsza wiadomość została wysłana przez Infinity Code (52-234 Wrocław, ul. Komedy 3/37, NIP 501-005-57-32,
                                    REGON 380761871. Administratorem Państwa danych osobowych jest Infinity Code Michał Rogalski. Są one przetwarzane
                                    i wykorzystane za pomocą środków komunikacji elektronicznej, wyłącznie w celu komunikacji.
                                </p>
                            </td>
                        </tr>
                        </tfoot>
                    </table>
                </center>
            </td>
        </tr>
    </table>
</table>
</body>
</html>
