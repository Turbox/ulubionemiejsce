@extends('layouts.guest')

@section('content')
    <section>
        <div class="content">
            <div class="flex">
                <div class="grid-8-8 m-grid-8-8">
                    <h2>Polityka prywatności</h2>
                    <h3><b>1.</b> POSTANOWIENIA OGÓLNE</h3>
                    <p><b>1.1.</b> Administratorem danych osobowych zbieranych za pośrednictwem strony internetowej ulubionemiejsce.pl jest Michał Rogalski wykonujący działalność gospodarczą pod firmą Infinity Code Michał Rogalski, adres siedziby: Komedy 3/37, 52-234 Wrocław, adres do doręczeń: Komedy 3/37, 52-234 Wrocław, NIP: 501-00-55-732, REGON: 380761871, wpisaną do Centralnej Ewidencji i Informacji o Działalności Gospodarczej, adres poczty elektronicznej: info@ulubionemiejsce.pl, dalej „Administrator”, będący jednocześnie Usługodawcą, miejsce wykonywania działalności: Komedy 3/37, 52-234 Wrocław, adres do doręczeń: Komedy 3/37, 52-234 Wrocław, NIP: 501-00-55-732, REGON: 380761871, adres poczty elektronicznej (e-mail): info@ulubionemiejsce.pl, zwany/a dalej "Administratorem".</p>
                    <p><b>1.2.</b> Dane osobowe zbierane przez Administratora za pośrednictwem strony internetowej są przetwarzane zgodnie z Rozporządzeniem Parlamentu Europejskiego i Rady (UE) 2016/679 z dnia 27 kwietnia 2016 r. w sprawie ochrony osób fizycznych w związku z przetwarzaniem danych osobowych i w sprawie swobodnego przepływu takich danych oraz uchylenia dyrektywy 95/46/WE (ogólne rozporządzenie o ochronie danych), zwane dalej RODO oraz ustawą o ochronie danych osobowych z dnia 10 maja 2018 r.</p>

                    <h3><b>2.</b> RODZAJ PRZETWARZANYCH DANYCH OSOBOWYCH, CEL I ZAKRES ZBIERANIA DANYCH</h3>
                    <p><b>2.1.</b> CEL PRZETWARZANIA I PODSTAWA PRAWNA. Administrator przetwarza dane osobowe za pośrednictwem strony <strong>ulubionemiejsce.pl</strong> w przypadku:
                        skorzystania przez użytkownika z formularza kontaktowego. Dane osobowe są przetwarzane na podstawie art. 6 ust. 1 lit. f) RODO jako prawnie usprawiedliwiony interes Administratora. zapisania się przez użytkownika do Newslettera w celu przesyłania informacji handlowych drogą elektroniczną. Dane osobowe są przetwarzane po wyrażeniu odrębnej zgody, na podstawie art. 6 ust. 1 lit. a) RODO.</p>
                    <p><b>2.2.</b> OKRES ARCHIWIZACJI DANYCH OSOBOWYCH. Dane osobowe użytkowników przechowywane są przez Administratora:</p>
                    <p><b>2.2.1.</b> w przypadku, gdy podstawą przetwarzania danych jest wykonanie umowy, tak długo, jak jest to niezbędne do wykonania umowy, a po tym czasie przez okres odpowiadający okresowi przedawnienia roszczeń. Jeżeli przepis szczególny nie stanowi inaczej, termin przedawnienia wynosi lat sześć, a dla roszczeń o świadczenia okresowe oraz roszczeń związanych z prowadzeniem działalności gospodarczej - trzy lata.</p>
                    <p><b>2.2.2.</b> w przypadku, gdy podstawą przetwarzania danych jest zgoda, tak długo, aż zgoda nie zostanie odwołana, a po odwołaniu zgody przez okres czasu odpowiadający okresowi przedawnienia roszczeń jakie może podnosić Administrator i jakie mogą być podnoszone wobec niego. Jeżeli przepis szczególny nie stanowi inaczej, termin przedawnienia wynosi lat sześć, a dla roszczeń o świadczenia okresowe oraz roszczeń związanych z prowadzeniem działalności gospodarczej - trzy lata.</p>
                    <p><b>2.3.</b> Podczas korzystania ze strony internetowej mogą być pobierane dodatkowe informacje, w szczególności: adres IP przypisany do komputera użytkownika lub zewnętrzny adres IP dostawcy Internetu, nazwa domeny, rodzaj przeglądarki, czas dostępu, typ systemu operacyjnego.</p>
                    <p><b>2.4.</b> Od użytkowników mogą być także gromadzone dane nawigacyjne, w tym informacje o linkach i odnośnikach, w które zdecydują się kliknąć lub innych czynnościach, podejmowanych na stronie internetowej. Podstawą prawną tego rodzaju czynności jest prawnie uzasadniony interes Administratora (art. 6 ust. 1 lit. f RODO), polegający na ułatwieniu korzystania z usług świadczonych drogą elektroniczną oraz na poprawie funkcjonalności tych usług.</p>
                    <p><b>2.5.</b> Podanie danych osobowych przez użytkownika jest dobrowolne.</p>
                    <p><b>2.6.</b> Dane osobowe będą przetwarzane także w sposób zautomatyzowany w formie profilowania, o ile użytkownik wyrazi na to zgodę na podstawie art. 6 ust. 1 lit. a) RODO. Konsekwencją profilowania będzie przypisanie danej osobie profilu w celu podejmowania dotyczących jej decyzji bądź analizy lub przewidywania jej preferencji, zachowań i postaw.</p>
                    <p><b>2.7.</b> Administrator dokłada szczególnej staranności w celu ochrony interesów osób, których dane dotyczą, a w szczególności zapewnia, że zbierane przez niego dane są:</p>
                    <p><b>2.7.1.</b> przetwarzane zgodnie z prawem,</p>
                    <p><b>2.7.2.</b> zbierane dla oznaczonych, zgodnych z prawem celów i niepoddawane dalszemu przetwarzaniu niezgodnemu z tymi celami,</p>
                    <p><b>2.7.3.</b> merytorycznie poprawne i adekwatne w stosunku do celów, w jakich są przetwarzane oraz przechowywane w postaci umożliwiającej identyfikację osób, których dotyczą, nie dłużej niż jest to niezbędne do osiągnięcia celu przetwarzania.</p>

                    <h3>3. UDOSTĘPNIENIE DANYCH OSOBOWYCH</h3>
                    <p><b>3.1.</b> Dane osobowe użytkowników przekazywane są dostawcom usług, z których korzysta Administrator przy prowadzeniu strony internetowej. Dostawcy usług, którym przekazywane są dane osobowe, w zależności od uzgodnień umownych i okoliczności, albo podlegają poleceniom Administratora co do celów i sposobów przetwarzania tych danych (podmioty przetwarzające) albo samodzielnie określają cele i sposoby ich przetwarzania (administratorzy).</p>
                    <p><b>3.2.</b> Dane osobowe użytkowników są przechowywane wyłącznie na terenie Europejskiego Obszaru Gospodarczego (EOG).</p>

                    <h3><b>4.</b> PRAWO KONTROLI, DOSTĘPU DO TREŚCI WŁASNYCH DANYCH ORAZ ICH POPRAWIANIA</h3>
                    <p><b>4.1.</b> Osoba, której dane dotyczą, ma prawo dostępu do treści swoich danych osobowych oraz prawo ich sprostowania, usunięcia, ograniczenia przetwarzania, prawo do przenoszenia danych, prawo wniesienia sprzeciwu, prawo do cofnięcia zgody w dowolnym momencie bez wpływu na zgodność z prawem przetwarzania, którego dokonano na podstawie zgody przed jej cofnięciem.</p>
                    <p><b>4.2.</b> Podstawy prawne żądania użytkownika:</p>
                    <p><b>4.2.1.</b> Dostęp do danych – art. 15 RODO</p>
                    <p><b>4.2.2.</b> Sprostowanie danych – art. 16 RODO.</p>
                    <p><b>4.2.3.</b> Usunięcie danych (tzw. prawo do bycia zapomnianym) – art. 17 RODO.</p>
                    <p><b>4.2.4.</b> Ograniczenie przetwarzania – art. 18 RODO.</p>
                    <p><b>4.2.5.</b> Przeniesienie danych – art. 20 RODO.</p>
                    <p><b>4.2.6.</b> Sprzeciw – art. 21 RODO.</p>
                    <p><b>4.2.7.</b> Cofnięcie zgody – art. 7 ust. 3 RODO.</p>

                    <p><b>4.3.</b> W celu realizacji uprawnień, o których mowa w pkt 2 można wysłać stosowną wiadomość e-mail na adres: info@ulubionemiejsce.pl.</p>
                    <p><b>4.4.</b> W sytuacji wystąpienia przez użytkownika z uprawnieniem wynikającym z powyższych praw, Administrator spełnia żądanie albo odmawia jego spełnienia niezwłocznie, nie później jednak niż w ciągu miesiąca po jego otrzymaniu. Jeżeli jednak - z uwagi na skomplikowany charakter żądania lub liczbę żądań – Administrator nie będzie mógł spełnić żądania w ciągu miesiąca, spełni je w ciągu kolejnych dwóch miesięcy informując użytkownika uprzednio w terminie miesiąca od otrzymania żądania - o zamierzonym przedłużeniu terminu oraz jego przyczynach.</p>
                    <p><b>4.5.</b> W przypadku stwierdzenia, że przetwarzanie danych osobowych narusza przepisy RODO, osoba, której dane dotyczą, ma prawo wnieść skargę do Prezesa Urzędu Ochrony Danych Osobowych.</p>

                    <h3><b>5.</b> PLIKI "COOKIES"</h3>
                    <p><b>5.1.</b> Strona Administratora używa plików „cookies”.</p>
                    <p><b>5.2.</b> Instalacja plików „cookies” jest konieczna do prawidłowego świadczenia usług na stronie internetowej. W plikach „cookies" znajdują się informacje niezbędne do prawidłowego funkcjonowania strony, a także dają one także możliwość opracowywania ogólnych statystyk odwiedzin strony internetowej.</p>
                    <p><b>5.3.</b> W ramach strony stosowane są rodzaje plików "cookies": sesyjne i stałe</p>
                    <p><b>5.3.1.</b> „Cookies” „sesyjne” są plikami tymczasowymi, które przechowywane są w urządzeniu końcowym użytkownika do czasu wylogowania (opuszczenia strony).</p>
                    <p><b>5.3.2.</b> „Stałe” pliki „cookies” przechowywane są w urządzeniu końcowym użytkownika przez czas określony w parametrach plików „cookies” lub do czasu ich usunięcia przez użytkownika.</p>
                    <p><b>5.4.</b> Administrator wykorzystuje własne pliki cookies w celu lepszego poznania sposobu interakcji użytkownika w zakresie zawartości strony. Pliki gromadzą informacje o sposobie korzystania ze strony internetowej przez użytkownika, typie strony, z jakiej użytkownik został przekierowany oraz liczbie odwiedzin i czasie wizyty użytkownika na stronie internetowej. Informacje te nie rejestrują konkretnych danych osobowych użytkownika, lecz służą do opracowania statystyk korzystania ze strony.</p>
                    <p><b>5.5.</b> Użytkownik ma prawo zadecydowania w zakresie dostępu plików „cookies” do swojego komputera poprzez ich uprzedni wybór w oknie swojej przeglądarki.  Szczegółowe informacje o możliwości i sposobach obsługi plików „cookies” dostępne są w ustawieniach oprogramowania (przeglądarki internetowej).</p>

                    <h3><b>6.</b> POSTANOWIENIA KOŃCOWE</h3>
                    <p><b>6.1.</b> Administrator stosuje środki techniczne i organizacyjne zapewniające ochronę przetwarzanych danych osobowych odpowiednią do zagrożeń oraz kategorii danych objętych ochroną, a w szczególności zabezpiecza dane przed ich udostępnieniem osobom nieupoważnionym, zabraniem przez osobę nieuprawnioną, przetwarzaniem z naruszeniem obowiązujących przepisów oraz zmianą, utratą, uszkodzeniem lub zniszczeniem.</p>
                    <p><b>6.2.</b> Administrator udostępnia odpowiednie środki techniczne zapobiegające pozyskiwaniu i modyfikowaniu przez osoby nieuprawnione, danych osobowych przesyłanych drogą elektroniczną.</p>
                    <p>W sprawach nieuregulowanych niniejszą Polityką prywatności stosuje się odpowiednio przepisy RODO oraz inne właściwe przepisy prawa polskiego.</p>
                </div>
            </div>
        </div>
    </section>
@endsection