@extends('layouts.guest')

@section('content')
    <section class="confirm">
        <div class="content">
            <div class="flex">
                <div class="grid-8-8 m-grid-8-8">
                    <h2>Cześć!</h2>
                    <p>Cieszymy się, że chcesz do nas dołączyć.</p>
                    <p>W twojej skrzynce czeka już wiadomość. Potwierdź subskrypcję i gotowe!</p>
                    <p>Polub nas na Facebooku lub Instagramie i bądź na bieżąco.</p>
                </div>
                <div class="grid-8-8 m-grid-8-8 ">
                    <div class="background-rainbow"></div>
                </div>
            </div>
        </div>
    </section>
@endsection