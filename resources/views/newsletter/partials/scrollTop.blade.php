@section('scrollTop')
    <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('html, body').animate({
                scrollTop: $("#newsletter-form").offset().top
            },1000);
        });
    </script>
@endsection