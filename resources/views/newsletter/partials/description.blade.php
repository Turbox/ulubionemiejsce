<section class="description">
    <div class="content">
        <div class="flex">
            <div class="grid-4-8 m-grid-8-8 ">
                <div class="background-webview-2"></div>
                <div class="background-webview-3"></div>
                <div class="background-webview-4"></div>
            </div>
            <div class="grid-4-8 m-grid-8-8 ">
                <h2>Już niedługo ruszamy!</h2>
                <p>Koronawirus wywrócił nam życie do góry do nogami. Jest to ciężki czas dla małych biznesów i ich właścicieli. Dla ludzi, którzy włożyli
                    mnóstwo pracy, by stworzyć <strong>twoje ulubione miejsce</strong>.
                    Teraz ono potrzebuje twojej pomocy. Jeśli masz takie miejsce i chcesz mu pomóc, to daj mu znać o naszej akcji.</p>

                <p>A może właśnie prowadzisz biznes, który dla innych jest takim miejscem? Tym ulubionym, do którego
                    przychodzą z uśmiechem na ustach, a które teraz z powodu pandemii stoi puste? Zapisz się i poinformuj swoich klientów, że mogą cię wesprzeć.</p>

                    <p>Tworzymy tę platformę dla was.</p>

                    <p><strong>Dla przedsiębiorców</strong>, którzy stworzyli miejsca, które żyją i do których
                        ludzie chcą wracać, ale ze względu na obecny kryzys potrzebują finansowego wsparcia.</p>

                    <p><strong>Dla klientów</strong>, którzy chcą wesprzeć i uratować swoje ulubione miejsca.</p>

                <h2>Razem możemy sobie pomóc! </h2>
            </div>
        </div>
    </div>
</section>