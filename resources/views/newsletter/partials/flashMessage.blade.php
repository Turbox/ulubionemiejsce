<div id="confirm-message">
    <div class="grid-4-8 m-grid-8-8">
        @if (Session::has('messages'))
            @foreach(Session::get('messages') AS $key => $message)
                @foreach ($message as $msg)
                    <div class="alert alert-{{$key}}">
                        <p class="alert-text">{{$msg}}</p>
                    </div>
                @endforeach
            @endforeach
        @endif
    </div>
</div>