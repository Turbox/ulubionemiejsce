<section>
    <div class="content">
        @include('newsletter.partials.flashMessage')
        <div class="flex">
            <div class="grid-4-8 m-grid-8-8">
                <h2>Zapisz się na listę, a my poinformujemy cię, kiedy strona będzie gotowa.</h2>
                <form id="newsletter-form" method="post" action="{{route('newsletter.send')}}" enctype="multipart/form-data">
                    @csrf
                    @if ($errors->has('accountType'))
                        <div class="alert-accountType alert alert-danger">
                            <p class="alert-text">{{ $errors->first('accountType') }}</p>
                        </div>
                    @endif
                    <div class="accountType">
                        <label for="accountType1">
                            <input id="accountType1" type="radio" name="accountType" value="1"
                                    {{ old('accountType') == 1 ? 'checked' : '' }} >
                            Jestem przedsiębiorcą
                        </label>
                        <label for="accountType2">
                            <input id="accountType2" type="radio" name="accountType" value="2"
                                    {{ old('accountType') == 2 ? 'checked' : '' }}>
                            Jestem klientem
                        </label>
                    </div>

                    @if ($errors->has('name'))
                        <div class="alert alert-danger">
                            <p class="alert-text">{{ $errors->first('name') }}</p>
                        </div>
                    @endif
                    <label for="name">Twoje imię</label>
                    <input id="name" type="text" placeholder="Wpisz swoje imię" name="name" value="{{ old('name') }}" required >

                    @if ($errors->has('email'))
                        <div class="alert alert-danger">
                            <p class="alert-text">{{ $errors->first('email') }}</p>
                        </div>
                    @endif
                    <label for="email">Twoj email</label>
                    <input id="email" type="text" placeholder="Wpisz swój adres E-mail" name="email" value="{{ old('email') }}" required>

                    <div class="agreement">
                        @if ($errors->has('approve'))
                            <div class="alert alert-danger">
                                <p class="alert-text">{{ $errors->first('approve') }}</p>
                            </div>
                        @endif
                        <input id="approve" name="approve" value="1" type="checkbox" {{old('approve') ? 'checked' : '' }}>
                        <label for="approve" class="approve">Wyrażam zgodę na przetwarzanie danych osobowych oraz otrzymywanie informacji handlowych związanych z projektem.
                            W każdej chwili możesz wycofać swoją zgodę. Szczegóły znajdziesz w <a href="{{ route('home.privacyPolicy') }}">polityce prywatności.</a>
                        </label>
                    </div>
                    <button id="sendMessage" name="buttonSend" value="true">Zapisuję się</button>
                </form>
            </div>
            <div class="grid-4-8 m-grid-8-8 ">
                <div class="background-webview-1"></div>
            </div>
        </div>
    </div>
</section>
@section('script')
    <script type="application/javascript">
        let input = 'input';

        $(input).keydown(function () {
            if ($(this).prev().prev('.alert')) {
                $(this).prev().prev('.alert').remove();
            }
        });

        $(input).click(function () {
            if ($(this).attr('id') === 'approve' && $(this).prev('.alert')) {
                $(this).prev('.alert').remove();
            }
        });

        $('.accountType').click(function(){
            let accountType = $('.alert-accountType');

            if(accountType.length !== 'undefined') {
                accountType.remove();
            }
        });
    </script>
@endsection