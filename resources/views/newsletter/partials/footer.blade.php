<footer>
    <section>
        <div class="content">
            <div class="flex ">
                <div class="grid-1-3 m-grid-1-2 m-grid-8-8">
                    <ul>
                        {{--<li><a href="#">Kontakt</a></li>--}}
                        {{--<li><a href="#">Regulamin</a></li>--}}
                        <li><a href="{{ route('newsletter.index') }}">Home</a></li>
                        <li><a href="{{ route('home.privacyPolicy') }}">Polityka prywatności</a></li>
                    </ul>
                </div>
                <div class="grid-1-3 m-grid-1-2 m-grid-8-8">
                    <div class="icon-flex">
                        <a target="_blank" href="https://www.facebook.com/projektulubionemiejsce" class="icon-url">
                            <div class="icon-content">
                                <i class="fa fa-facebook" aria-hidden="true"></i>
                            </div>
                        </a>
                        <a href="mailto:info@ulubionemiejsce.pl" class="icon-url">
                            <div class="icon-content">
                                <i class="fa fa-envelope-o" aria-hidden="true"></i>
                            </div>
                        </a>
                        <a target="_blank" href="https://www.instagram.com/ulubionemiejsce/" class="icon-url">
                            <div class="icon-content">
                                <i class="fa fa-instagram" aria-hidden="true"></i>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="grid-1-3 quote">
                    <p>Wspierając</p>
                    <p><span>budujesz lepsze jutro</span></p>
                </div>
            </div>
            <div class="copyright">Copyright © 2020 Projekt Korona</div>
        </div>
    </section>
</footer>