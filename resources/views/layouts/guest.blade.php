<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no, target-densityDpi=device-dpi" />
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Twoje ulubione miejsce</title>
        <link href="{{ mix('css/guest.css') }}" type="text/css" rel="stylesheet">
        <script src="https://use.fontawesome.com/77ba9dd44c.js"></script>
        @include('analytics')
    </head>
    <body>
        <div id="app">
            <header id="header">
                @include('newsletter.partials.header')
            </header>
            <main>
                @yield('content')
            </main>
            @include('newsletter.partials.footer')
        </div>
        <script src="{{ mix('js/app.js') }}"></script>
        @yield('mainScript')
        @yield('script')
    </body>
</html>
