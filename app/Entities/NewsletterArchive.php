<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class NewsletterArchive extends Model
{

    protected $table = 'newstletter_archive';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['accountType', 'name', 'email'];

    public $timestamps = true;
}
