<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Newsletter extends Model
{

    protected $table = 'newsletter';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['accountType','name','email','status','verificationHash'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'verificationHash'
    ];

    public $timestamps = true;
}
