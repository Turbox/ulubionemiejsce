<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth;
use App\Entities\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function username($login = null) {
        return 'email';
    }

    protected function validator(array $data) {
        Validator::extend('canLogin', function($attribute, $value, $parameters) use ($data){

            if(User::where('email',$data[$this->username()])
                   ->where('statusId',2)
                   ->count() > 0) {
                return true;
            }

            return false;
        });

        return Validator::make($data,
            [
                $this->username() => [
                    'required',
                    'string',
                    'can_login'
                ],
                'password' => 'required|string'
            ],
            [
                '*' => 'Login lub hasło są nieprawidłowe',
            ]
        );
    }

    public function credentials(Request $request) {
        $arrCredentials = $request->only($this->username(),'password');
        $arrCredentials['email'] = $arrCredentials[$this->username()];
        return $arrCredentials;
    }

    protected function validateLogin(Request $request) {
        $this->validator($request->toArray())->validate();
    }

    public function login(Auth $request) {

        $this->validateLogin($request);

        if($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            $user = \Illuminate\Support\Facades\Auth::user();
            $user->lastLoginDate = date('Y-m-d H:i:s');
            $user->save();
            return $this->sendLoginResponse($request);
        }

        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    public function dashboard() {
        return view('auth.dashboard');
    }
}
