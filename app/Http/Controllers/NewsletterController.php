<?php

namespace App\Http\Controllers;

use App\Entities\Newsletter;
use App\Entities\NewsletterArchive;
use App\Http\Controllers\Controller;
use App\Http\Requests\Newsletter AS RequestNewsletter;
use Illuminate\Support\Facades\Mail;

class NewsletterController extends Controller {

    public function index() {
        return view('newsletter.index');
    }

    public function confirm($flag = null) {
        if(!$flag) {
            return redirect()->route('newsletter.index');
        }

        return view('newsletter.confirm');
    }

    public function verificationEmailAddress($hash, $action) {
        $oMessageBag = new \Illuminate\Support\MessageBag;

        $message = 'Ok';
        switch($action) {
            case 'confirm':
                $oldStatus = 1;
                $newStatus = 2;
                $message = 'Twój email został potwierdzony.';
                break;

            case 'unsub':
                $oldStatus = 2;
                $newStatus = 9;
                $message = 'Zostałeś wypisany z newslettera.';
                break;
        }

        $oNewsletter = Newsletter::where(['verificationHash' => $hash, 'status' => $oldStatus])->first();

        if($oNewsletter) {
            if($newStatus == 2) {
                $oNewsletter->status = $newStatus;
                $oNewsletter->save();
            } else {
                NewsletterArchive::firstOrCreate(
                    [
                        'accountType' => $oNewsletter->accountType,
                        'name' => $oNewsletter->name,
                        'email' => $oNewsletter->email
                    ]
                );
                $oNewsletter->delete();
            }

            $oMessageBag->add('information', $message);
            return redirect()->route('newsletter.index')->with('messages', $oMessageBag->getMessages());
        }

        $oMessageBag->add('danger', 'Podany email nie istnieje w naszej bazie, bądź link stracił ważność!');

        return redirect()->route('newsletter.index')->with('messages', $oMessageBag->getMessages());
    }

    public function send(RequestNewsletter $request) {

        $aTemplateName = [
            1 => 'company',
            2 => 'customer',
        ];

        $messages = new \Illuminate\Support\MessageBag;
        $hash = sha1(uniqid(rand(900,999), true));

        if(!Newsletter::where(['email' => $request->email])->exists()) {
            $post = $request->all();

            $post['verificationHash'] = $hash;
            $post['status'] = 1;
            unset($post['_token']);
            unset($post['approve']);
            unset($post['buttonSend']);

            $oNewsletter = Newsletter::firstOrCreate($post);
        } else {
            $messages->add('danger', __('email.emailExists'));

            return redirect()->back()->with('messages', $messages->getMessages());
        }

        if($oNewsletter) {
            Mail::send('template.newsletter.' . $aTemplateName[$post['accountType']], ['request' => $request, 'hash' => $hash], function ($m) use ($request) {
              $m->from('info@ulubionemiejsce.pl', 'Ulubionemiejsce.pl');

              $m->to($request->email, $request->name)->subject('Potwierdź zapis na listę.');
          });
        }

        $messages->add('information', __('email.confirmMessage'));

        return redirect()->route('newsletter.confirm', [true])->with('messages', $messages->getMessages());
    }

    public function test() {
//        return true;

        $hash = sha1(uniqid(rand(900,999), true));

        Mail::send('template.newsletter.company', ['hash' => $hash], function ($m)  {
            $m->from('info@ulubionemiejsce.pl', __('email.messageFrom'));

            $m->to('okarmus.p@gmail.com', 'Test email')->subject(__('email.messageSubjectConfirm'));
//            $m->to('maxsheep@gmail.com', 'Test email')->subject(__('email.messageSubjectConfirm'));
        });

        dd('poszlo');
    }

}
