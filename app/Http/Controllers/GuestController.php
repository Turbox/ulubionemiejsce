<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\Contact;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

class GuestController extends Controller {

    public function newsletter() {
        return view('guest.newsletter');
    }

    public function send(Contact $request) {
//        $post = $request->all();

        $insert = DB::table('newsletter')->insertGetId(
            [
                'accountType' => $request->accountType,
                'name' => $request->name,
                'email' => $request->email,
                'verificationHash' => Hash::make(Str::random(10)),
                'status' => 1,
            ]
        );

        if($insert) {
            Mail::send('template.newsletter', ['post' => $request], function ($m) use ($request) {
              $m->from('info@ulubionemiejsce.pl', __('email.messageFrom'));

              $m->to($request->email, $request->name)->subject(__('email.messageSubject'));
          });
        }

        $messages = new \Illuminate\Support\MessageBag;
        $messages->add('information', __('email.confirmMessage'));

        return redirect()->back()->with('messages', $messages->getMessages());
    }

}
