<?php

namespace App\Http\Controllers;

use App\Entities\Newsletter;
use App\Http\Controllers\Controller;
use App\Http\Requests\Newsletter AS RequestNewsletter;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller {

    public function privacyPolicy() {
        return view('home.privacyPolicy');
    }

}
