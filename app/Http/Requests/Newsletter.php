<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Newsletter extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'accountType' => 'required',
            'name' => 'required|max:80',
            'email' => [
                'required',
                'email',
                'max:80'
            ],
            'approve' => 'required',
        ];
    }

    public function messages() {
        return [
            'accountType.required' => 'Musisz wybrać typ konta.',
            '*.min' => 'Minimalna dopuszczalna ilość znaków to :min',
            '*.max' => 'Maxymalna dopuszczalna ilość znaków to :max',
            'name.required' => 'Pole Imię jest wymagane.',
            'email.required' => 'Pole E-mail jest wymagane',
            'email.email' => 'E-mail musi miec poprawny format.',
            'approve.required' => 'Wymagane jest zakceptowanie zgody marketingowej.',
        ];
    }
}
