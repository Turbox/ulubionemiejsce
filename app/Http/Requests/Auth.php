<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Auth extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'email' => 'required',
            'password' => 'required',
        ];
    }

    public function messages() {
        return [
            'email.digits' => 'Login i/lub hasło są nieprawidłowe',
            'email.email' => 'Login i/lub hasło są nieprawidłowe',
            'email.required' => 'Proszę podać login',
            'password.required' => 'Proszę podać hasło',
        ];
    }
}
