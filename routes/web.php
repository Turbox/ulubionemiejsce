<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Auth::routes();

Route::middleware(['guest'])->group(function() {

    Route::get('en', function() {
        session(['locale' => 'en']);
        return back();
    })->name('locale.english');

    Route::get('pl', function() {
        session(['locale' => 'pl']);
        return back();
    })->name('locale.polish');

    Route::get('', function (){
        return redirect()->route('newsletter.index');
    })->name('guest.index');

    Route::group([
        'prefix' => 'newsletter'
    ], function() {
        Route::get('', 'NewsletterController@index')->name('newsletter.index');
        Route::post('', 'NewsletterController@send')->name('newsletter.send');

        Route::get('confirm/{flag?}', 'NewsletterController@confirm')->name('newsletter.confirm');
        Route::get('check/{hash}/action/{action}', 'NewsletterController@verificationEmailAddress')->name('newsletter.check');
    });

    Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
    Route::post('login', 'Auth\LoginController@login')->name('login');
    Route::get('/privacy-policy','HomeController@privacyPolicy')->name('home.privacyPolicy');

});

Route::middleware(['auth'])->group(function() {
//    Route::get('test','TestController@index')->name('auth.test');

    Route::get('dashboard','AuthController@index')->name('auth.dashboard');
    Route::get('logout','Auth\LoginController@logout')->name('auth.logout');
});