<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewsletter extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('newsletter', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('accountType')->unsigned();
            $table->string('name',60);
            $table->string('email', 60)->unique();
            $table->string('verificationHash',60);
            $table->integer('status')->unsigned()
                                     ->comment('0 - niepotwierdzony, 1 - potwierdzony, 9 - nieaktywny');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('newsletter');
    }
}
