<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class User extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('user', function(Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('email', 80)->unique()->nullable();
            $table->string('password', 80)->nullable();
            $table->string('firstName', 80)->nullable();
            $table->string('lastName', 80)->nullable();
            $table->integer('phoneNumber')->nullable();
            $table->rememberToken()->nullable();
            $table->integer('roleId')->unsigned()->nullable();
            $table->integer('statusId')->unsigned()->nullable();
            $table->integer('userId')->nullable()->unsigned();
            $table->timestamp('lastLoginDate')->nullable();
            $table->timestamps();

            $table->foreign('userId')
                  ->references('id')
                  ->on('user');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Schema::dropIfExists('user');
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
